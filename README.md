 **layer移动端二次封装了几个常用的弹窗** 
为别为alert,msg,confirm,loading,success,error
具体可看源码，非常简单。<br>

具体使用方法：下载源码到项目目录，引入layer.js<br>
 **使用方法：** <br>
ui.msg('这是一段文本');<br>
ui.alert('这是一个弹窗');<br>
具体的回调方法可看 layer.js文件中的源码<br>

图片示例：<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0622/092612_7b441150_2664028.png "QQ截图20210622092519.png")<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0622/092624_9a5d617a_2664028.png "QQ截图20210622092528.png")<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0622/092630_d83f8e3d_2664028.png "QQ截图20210622092536.png")<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0622/092637_20efb4c4_2664028.png "QQ截图20210622092547.png")<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0622/092642_3ba1debe_2664028.png "QQ截图20210622092554.png")<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0622/092648_70ac2a1c_2664028.png "QQ截图20210622092559.png")<br>
